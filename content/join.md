+++
title = "Join"
template = "page.html"
aliases = ["contact"]
+++

If you know [Louis Birla](https://birla.io) or another member, contact them for an invitation! If you don't know Louis, don't hesitate to contact him anyway.
# Copied from my personal website, removed IPFS
# More at birla.io/questions/how-is-this-site-run
# Just in case git
git add .
git commit -m "backup"
# Generate the files
zola build
# Move the build out of the repository
mkdir ../temp-ovo-site
rm ../temp-ovo-site/*
mv public/* ../temp-ovo-site/
# Checkout to pages
git checkout pages
# Delete the existing build (.domains is kept)
rm -rf *
# Move the build in
mv ../temp-ovo-site/* .
# Add and commit
git add .
git commit -m "publish script"
git push origin pages
# Checkout back to main
git checkout main
#let title = "Promenade Proposal"
#let zulip_link = "https://ovo.zulipchat.com/join/md6chzg73du2el73ftikqnro/"
#set document(
  title: title,
  author: "Louis Birla"
  )
#set text(
  size: 14pt,
)
#set par(justify: true)
#set page(
  paper: "a4",
  numbering: "1",
)
#set quote(
  block: true
)

#show link: set text(blue)
#show link: underline

#show heading.where(
  level: 2
): it => align(center, it)

#align(center)[
  #image("owl_of_minerva.png", height: 128pt)
  = #title
  #text(size: 11pt)[
    Fri, 19 April 2024 • #link("https://birla.io/")[Louis Birla] • v0.4.0
  ]
]

#quote(attribution: [Laozi (mostly)])[
  A _Promenade_ of a thousand miles begins with a single step.
]

Learning and thinking are some of my favorite things in this world, which manifest as strong interest in mathematics, philosophy, and science, for example. I don't find these curiosities fully satisfied in my major courseload, and there is no way for me to simply sign up for courses that interest me.

Last semester, I found out about the Philosophy Club #footnote[This project is not related to the Philosophy Club in any official capacity.], which has turned out to be a wonderful place to talk about amazing subjects, meet wonderful people, and ask deep questions.

Recently in the club there has been some enthusiasm voiced for longer, more serious consideration of philosophical issues. In response, one officer pointed out that the current nature of the club is exactly what enables it's accessibility and active participation:

#quote(attribution: [Sawyer Rabalais])[
  I understand what’s being said but I believe that what the Philosophy Club thrives on is that there is such a focus on discussion rather than lecture, something you can’t always find in your average philosophy class.
]

I believe in that, and do not wish for the Philosophy Club to lose it's chaotic and fun style. However, I still feel that I need something more.

That _something more_ is what I am calling "Promenade," a group for wonderers devoted to free and open education. In the rest of this document, I outline the organization in informal language. If there is enough interest in the establishment of such a group, I will lead a collaborative effort to bring it to reality.

In the meantime, please #link("https://birla.io/contact")[contact me directly] or #link(zulip_link)[join the organizational chat on Zulip] #footnote[I chose Zulip for it's feature-set (see streams and topics), the fact that it is open source and self-hostable, and that it is available online without any download.] <zulip> to help in the brainstorming of this enterprise.

== Lay of the Land
#align(center)[#text(size: 11pt)[
  _A bird's eye view of the vision_
]]

The fundamentals of Promenade came from the ideas of _Setu Gupta_. In relation to the discussion mentioned above, he said:

#quote(attribution: [Setu Gupta])[
  Maybe we can meet more often or for more time to discuss the topic at length. [...] Maybe an alternative could be to pick a topic and discuss various aspects of it over multiple meetings.
]

To be able to talk about and truly disagree or agree with arguments, we must understand them. But this cannot feasibly happen over a single hour. Multiple meetings, with time in between them to reflect and research independently, seems to be exactly what is missing.

In whicher way we decide to arrange this organization, certain meetings will be topic-bound and progress from the discussions of previous meetings. I will call these groups of meetings *courses*, and the individual meetings will mostly be *lectures*, *seminars*, and *workshops* led by *lecturers* who know the topic better than the *attendees*. You will notice right away that we are somewhat re-discovering the model used in universities, but there are key differences:

+ These courses will be entirely free of cost.
+ There is no requirement on attendees to continue attendance once they lose interest; there is no external detriment to dropping out.
+ The course topics are not dictated by a department or university; the lecturers themselves may lead discussion on whatever they please.
+ These courses will be lead, mostly, by those without professional background.

This is, in essence, _students teaching students_.

=== Free to Learn, Free to Teach
#text(size: 11pt)["The person who says he knows what he thinks but cannot express it usually does not know what he thinks." —Mortimer Adler]

I'm not an expert in anything, but I have some practice in a wide range of things. Small skills, like folding origami dragons or making websites---I believe everybody has knowledge like that. I'm not proficient enough to be a "professor of software engineering" or "master of origami," but I can absolutely show people how to create their own webpages or folded cranes.

Do you have an interesting philosophical argument brewing in your mind, but not enough substance to write a paper about it? Give a short lecture and let the in-depth discussion help you develop it, simultaneously stimulating the imagination of the attendees.

When it comes to philosophy, I'm just a beginner. Since I love it so much, I learn as much as I can, but I am also full of ideas. What better way to find the flaws and strengths in my thinking than to share it with people? That is what Promenade is about: learning from and teaching the same set of people.

How then are we to determine which members (of Promenade) have the ability to lecture on certain topics?

=== The Source of Expertise
When first starting out, I think it would be best to keep things simple and rely simply on personal experience and word-of-mouth. However, as it grows, it might be worth introducing the concept of *degrees of expertise*.

The foundation of Promenade rests on an assumption that #quote[it is worthwile to learn from somebody even if they only know a little bit more than you.]

If I know my way around concepts of reasoning and decide to take a course on formal logic, I don't want the lecturer to know _less_ than me. But, even if they only know _propositional logic_, which (in this example) I don't, the lecture would be worthwile. Perhaps, at some point, I feel as though I have learned everything I could on the topic from the lecturer---since there is no penalty for dropping out, I don't have to sacrifice my time.

Suppose another attendee knows a lot about formal logic and is there simply because they love the topic and would like to hear the perspective of the lecturer. If the lecturer was making things up, surely this person would be able to tell! On the other hand, if the attendee determines that the lecturer is in fact knowledgeable, the _degree of expertise_ for the lecturer in formal logic should be updated.

I will not detail exactly how members gain degrees of expertise in this document: they won't be implemented at the start. The important thing to know for now is that the higher a member's degree, the more likely they are to be an expert on a *topic*. That last word is crucial to this picture.

=== Topics

I think it is pretty clear that knowledge cannot be measured by a single number. The more specific the domain of knowledge, however, the more a number can represent something real. These "domains of knowledge" are called *topics*. Expertise can only exist in relation to a topic.

Examples of topics include _epistemology_, _geometry_, _continental philosophy_, and _mathematics_. It is clear that these are all topics, but how are they related? Clearly geometry is a sub-topic of mathematics, but epistemology isn't strictly speaking a sub-topic of continental philosophy!

We don't need to answer that question in it's entirety since we are only interested in figuring out who can talk about what. If I am familiar with geometry, it seems like I should be credible in mathematics as well. For now, this can be left simply to inference by members. In the future, degrees of expertise may resolve this problem.

While I will be most interested in learning philosophy, I do not want to limit the range of topics of courses and lectures. They don't even need to be academic, but practical skills, such as teaching people how to make simple websites. As long as there is interest and participation, why not?

Now, how would this all work?

=== The Logistics

Once there is enough interest in this project, I will begin work on a web app that will allow the proposals of courses, ways of showing interest both in courses themselves but also in topics, and much more.

If you are familiar with development and would like to help out, #link("https://birla.io/contact")[reach out] so that we can collaborate. As the project goes along, I could also give courses on how the infrastructure works and how to contribute to it.

For now, however, there is a way to ask questions and point out flaws on #link(zulip_link)[Zulip] @zulip, which we will use for communication.

== A Hypothetical Example

Suppose I have just found out about Promenade and want to attend a lecture a friend is giving. I visit the Promenade website to sign up for her course, which is about Inference to the Best Explanation (IBE). I search through the open courses and find it open for registration. There is a description of how the course will be carried out: four meetings, one every two weeks, with weekly optional seminars over video call. It is described as going over the history of scientific methods for a few courses until laying out and criticizing IBE at the last two meetings.

The first course has no knowledge prerequisite, but the rest require having taken the preceding lectures. Great, I sign up and #link(zulip_link)[join Promenade's Zulip chat], then subscribe to the course's stream. Surfing the streams (and figuring out how to use Zulip) I learn that the group is holding a social the day after my first lecture.

A few days, later it's the day of the first lecture. The majority of the lecture is my friend's presentation. After each big idea, she gives some time to talk with the people around to get a better grasp on the topic. The person to my right, I find out, knows a lot about the subject. He says wants to learn and talk about the criticisms to IBE, and even though he didn't have to, he attended this first lecture. Near the conclusion of the lecture, my friend starts a Kahoot! game to see how much people remember. Quaint, but my new friend wins it all.

The next day, it's the social. This time we are meeting in the MSC to talk about a milestone in membership and getting acquainted. I find out that a lot of people are very interested in learning the basics of programming, which I am confident in. Other members usually host these courses, but they are all too busy to offer them this semester. I message the topic stream on Zulip, gauging the interest if I were to start teaching a course.

Next week, I register a course on the website. I give a title, description, the topics, and a few other details. Other people with programming expertise confirm the topics are correct. A few people decide to sign up, including the knowledgeable friend I met at my first lecture.

A month later, I am at the last of my friend's lecture on IBE. Last lecture she outlined IBE in-depth and had us write a 1-page summary to ensure we could contribute to the discussion. Speaking of the discussion, it goes very well, and the person (who has always been) on my right had very interesting ideas.

The following morning, I leave a review of the course and on the teaching capabilities of my friend (she has done a great job). I will conduct my first lecture in three days, and cannot wait.

This concludes my messy example. It's not perfect, but I think it gives a good idea of what I have in mind.

== Details

In an earlier version of this draft, I had written more about degrees of expertise. However, they seemed overly complicated, especially when first beginning this project.

If the project grows and they seem more and more necessary, we will introduce them slowly and with the concensus of the members.

=== Organizational Structure

I want this group to be run as communally and "philosophically cogent" as possible. As such, founding members should work together to refine how the organization will be run, what it's rules will be (how can we prevent discrimination based on race, gender identity, etc., for example) and so on.

At first, I will be the interim president of the organization. Until we work together to hammer the details (including how to elect an actual president) I plan on steering the ship. After all, I typed up this beautiful document, didn't I?

== You are Invited

You are welcome to share this document to those you think will be interested. As we are just getting started, it might be wise to hold back on advertising to those likely to join only when things are better fleshed out.

If you contribute to this document or the ideas supporting it, I will, with permission, add you as an author or editor!

Please #link(zulip_link)[join the Zulip] @zulip so that we can get this going, or share this with an intellectually curious friend.

Thanks for reading!

#text(size: 11pt)[If you just scrolled to the bottom without reading it through, I don't blame you, but I rescind my thanks. :)]

#align(bottom)[
  #line(length: 100%)
  #set text(size: 11pt)
  This document does not necessarily represent the opinions of all the Promenade members.

  The "Owl of Minerva" graphic is by AlexanderAbelard on DeviantArt.
]